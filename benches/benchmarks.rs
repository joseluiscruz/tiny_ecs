#![allow(dead_code)]
use bencher::{benchmark_group, benchmark_main, Bencher, black_box};
use tiny_ecs::Entities;

/// Entities with velocity and position component.
/// Used to insert in staggered order
pub const N_POS_VEL_MODULUS: usize = 10;

/// Entities with position and velocity component
pub const N_POS_VEL: usize = 1_000;

/// Entities with position component only
pub const N_POS: usize = 10_000;

#[derive(Copy, Clone, Debug, PartialEq, Default)]
pub struct Position {
    pub x: f32,
    pub y: f32,
}

#[derive(Copy, Clone, Debug, PartialEq, Default)]
pub struct Velocity {
    pub dx: f32,
    pub dy: f32,
}

fn build() -> Entities {
    let mut entities = Entities::new(Some(N_POS), Some(2));

    for _ in 0..N_POS - N_POS_VEL {
        let x = entities
            .new_entity()
            .with(Position { x: 1.1, y: 2.2 })
            .unwrap()
            .finalise()
            .unwrap();
    }
    for _ in 0..N_POS_VEL {
        entities
            .new_entity()
            .with(Position { x: 1.3, y: 1.12 })
            .unwrap()
            .with(Velocity { dx: 5.0, dy: 1.4325 })
            .unwrap()
            .finalise()
            .unwrap();
    }
    entities
}

fn bench_update_safe(b: &mut Bencher) {
    let world = build();

    let vel = world.borrow::<Velocity>().unwrap();
    let mut pos = world.borrow_mut::<Position>().unwrap();

    let mut accum = 0.0;
    b.iter(|| {
        black_box(for (id, vel) in vel.iter().enumerate() {
            if let Some(pos) = pos.get_mut(id) {
                pos.x += vel.dx;
                pos.y += vel.dy;
                accum += vel.dy;
            }
        }
        )
    });
}

fn bench_update_unsafe(b: &mut Bencher) {
    let world = build();

    let vel = unsafe { world.borrow_unchecked::<Velocity>().unwrap() };
    let pos = unsafe { world.borrow_mut_unchecked::<Position>().unwrap() };

    let mut accum = 0.0;
    b.iter(|| {
        for (id, vel) in vel.iter().enumerate() {
            if let Some(pos) = pos.get_mut(id) {
                pos.x += vel.dx;
                pos.y += vel.dy;
                accum += vel.dy;
            }
        }
    });
}

fn bench_build(b: &mut Bencher) {
    b.iter(|| build());
}


benchmark_group!(
    benches,
    bench_build,
    bench_update_safe,
    bench_update_unsafe,
);
benchmark_main!(benches);
