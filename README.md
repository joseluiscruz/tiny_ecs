[![Build Status](https://drone.ljones.dev/api/badges/luke/tiny-ecs/status.svg)](https://drone.systemscoder.nz/luke/tiny-ecs)

# Tiny ECS

The intention of this crate is that a basic ECS is provided, where
you will be required to exercise a little additional control.

Where most other ECS crates provide a mechanism for inserting "systems"
in to the ECS to run against entities, this one leaves it out - you can
think of it as a "system for entity/components". You will need to create
external systems; these can be a function, a loop, or anything else.

Internally this ECS is the use of `bitmasks`. Each entity ID is in
practice an internal index number in to an array which contains bitmasks.
The bitmasks themselves keep track of what components the entity has.

**Note:** borrows of items are checked at runtime, but there are also
some *unchecked* borrows provided.

## Memory use

The ECS is backed by [Persist-O-Vec](https://crates.io/crates/persist-o-vec/) which
is a storage that aims to have fast push, pop, insert, and remove while also keeping
stable indexes and memory locations. Because of the way this works, and the fact that
not every slot in the storage will be used the memory-to-use ratio can be high.

The use of Persist-O-Vec means that each component will use an additional 24 bytes,
this includes 24 bytes per empty slot also.

For each component type there will be allocated an `Persist` store equal to the expected
maximum entity count. Slots in that storage remain allocated in memory throughout the
life of the storage, but may or may not actually contain data.

## Benchmarks

Based on the [ecs_bench](https://github.com/lschmierer/ecs_bench) project.

![](misc/bench.png)

## Alternatives

If you're looking for something a bit more robust, just as
fast, and cutting edge then one of the following may better better for you:

- [hecs](https://crates.io/crates/hecs)
- [legion](https://crates.io/crates/legion)
- [specs](https://crates.io/crates/specs)

## License

MPL v2
