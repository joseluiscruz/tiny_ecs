use std::error::Error;
use std::fmt;
use std::fmt::{Debug, Display};

#[derive(Debug)]
pub enum ECSError {
    Borrow,
    BorrowMut,
    Downcast,
    DowncastMut,
    PtrRef,
    PtrMut,
    NoComponentMap,
    NoComponentForEntity,
    WithAfterFinalise,
    FinaliseNonEntity,
    BitMasksExhausted,
    AddDupeComponent,
}

impl Display for ECSError {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ECSError::Borrow => Display::fmt("already borrowed", f),
            ECSError::BorrowMut => Display::fmt("already mutably borrowed", f),
            ECSError::Downcast => Display::fmt("could not downcast to ref", f),
            ECSError::DowncastMut => Display::fmt("could not downcast to mut ref", f),
            ECSError::PtrRef => Display::fmt("failed to convert pointer to ref", f),
            ECSError::PtrMut => Display::fmt("failed to convert pointer to mut", f),
            ECSError::NoComponentMap => Display::fmt("no component map for type", f),
            ECSError::NoComponentForEntity => {
                Display::fmt("part map does not contain part for entity", f)
            }
            ECSError::WithAfterFinalise => Display::fmt("use of .with() after .finalise()", f),
            ECSError::FinaliseNonEntity => {
                Display::fmt("can not finalise() without new_entity()", f)
            }
            ECSError::BitMasksExhausted => {
                Display::fmt("bitmasks exhausted, can't add new component type", f)
            }
            ECSError::AddDupeComponent => {
                Display::fmt("attempted to add duplicate component to entity", f)
            }
        }
    }
}

impl Error for ECSError {}
