use tiny_ecs::{ECSError, Entities};

struct Test1 {}
struct Test2 {}

fn set_up_entity_two_components() -> Result<Entities, ECSError> {
    let mut entities = Entities::new(Some(3), Some(3));
    let entity_1 = entities
        .new_entity()
        .with(Test1 {})?
        .with(Test2 {})?
        .finalise()?;
    assert_eq!(entity_1, 0);
    assert!(entities.entity_exists(0));
    assert!(entities.entity_contains::<Test1>(0));
    assert!(entities.entity_contains::<Test2>(0));
    Ok(entities)
}

#[test]
fn two_mutable_borrows_is_err() {
    let entities = set_up_entity_two_components().unwrap();
    let mut _p = entities.borrow_mut::<Test2>().unwrap();
    assert!(entities.borrow_mut::<Test2>().is_err());
}

#[test]
fn immutable_and_mutable_borrows_is_err() {
    let entities = set_up_entity_two_components().unwrap();
    let mut _p = entities.borrow_mut::<Test2>().unwrap();
    assert!(entities.borrow::<Test2>().is_err());
}

#[test]
fn immutable_and_unchecked_borrow_is_ok() {
    let entities = set_up_entity_two_components().unwrap();
    let _p = entities.borrow::<Test2>().unwrap();
    unsafe {
        assert!(entities.borrow_unchecked::<Test2>().is_ok());
    }
}

#[test]
fn immutable_and_unchecked_mut_borrow_is_ok() {
    let entities = set_up_entity_two_components().unwrap();
    let _p = entities.borrow::<Test2>().unwrap();
    unsafe {
        assert!(entities.borrow_mut_unchecked::<Test2>().is_ok());
    }
}

#[test]
fn mutable_and_unchecked_borrow_is_ok() {
    let entities = set_up_entity_two_components().unwrap();
    let mut _p = entities.borrow_mut::<Test2>().unwrap();
    unsafe {
        assert!(entities.borrow_unchecked::<Test2>().is_ok());
    }
}

#[test]
fn mutable_and_unchecked_mut_borrow_is_ok() {
    let entities = set_up_entity_two_components().unwrap();
    let mut _p = entities.borrow_mut::<Test2>().unwrap();
    unsafe {
        assert!(entities.borrow_mut_unchecked::<Test2>().is_ok());
    }
}
